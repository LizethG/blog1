class PostsController < ApplicationController

    #protect_from_forgery :except => [:index, :create]
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :auth_user!  
       
  def index      
     if admin_signed_in?
          @posts = Post.all
          respond_to do |format| 
              format.json do 
                  render :json => @posts, :callback => params[:callback] 
              end 
              format.html
         end
     end
      if user_signed_in? 
         @posts = current_user.posts
          respond_to do |format|
             format.json do 
                render :json => @posts, :callback => params[:callback] 
             end 
              format.html
          end
      end
        

  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit

      if user_signed_in?
      if cannot? :update, @posts
          flash[:error]="No tiene permisos para editar"
          redirect_to :action => 'index'
      end
      end
      
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    @post.user_id = current_user.id

    respond_to do |format|
      if @post.save
         # Correo.bienvenida().deliver_now
          flash[:success]="Se guardo el post correctamente"
          format.html{ redirect_to @post}
          #format.html { redirect_to @post, notice: 'El post se ha creado satisfactoriamente.' }
        format.json { render :show, status: :created, location: @post }
        format.json do
            render :json => @post, :callback => params[:callback]
        end
      else
          flash[:alert]='Ocurrio un problema al crear el post'


    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update

      #Verificar si el usuario tiene permisos para editar
      
      
    respond_to do |format|
      if @post.update(post_params)
          flash[:success]="Se actualizo correctamente"
        format.html { redirect_to @post }
        format.json { render :show, status: :ok, location: @post }
      else
          flash[:alert]="No se actualizo :C"

    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|

        flash[:info]= "El posts se borro correctamente"
        format.html{redirect_to posts_url}
      #format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }

      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }

      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params

        params.require(:post).permit(:tittle, :body, :categoria, :photo)
    end
end

      params.require(:post).permit(:title, :body)
    end
end
