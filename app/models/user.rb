class User < ActiveRecord::Base
   devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :posts, dependent: :destroy
    
    attr_accessor :photo
    FOTOPERFIL = File.join Rails.root, 'public', 'photo'
    after_save :guardar_foto
    
    def photo=(file_data)
        unless file_data.blank?
            @file_data = file_data
            self.extension_user = file_data.original_filename.split('.').last.downcase 
           
        end
    end
    
    def photo_filename 
        File.join FOTOPERFIL, "#{id}.#{extension_user}"
    end
    
    def photo_path
        "/photo/#{id}.#{extension_user}"
    end    
    
    def has_photo?
        File.exists? photo_filename
    end
    
    
    private
    def guardar_foto
        if @file_data 
            FileUtils.mkdir_p FOTOPERFIL
            File.open(photo_filename, 'wb') do |f| 
                f.write(@file_data.read)
                 
            end
            @file_data = nil
            
        end        
    end
end
