class Ability
  include CanCan::Ability

  def initialize(user)
      
      # Permisos para que el usuario pueda leer, eliminar & crear un posts
      can [:create, :destroy, :index], :all
      
      
    # Define abilities for the passed in user here. For example:
    #
      #Verifica si el usuario esta logeado 
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
  end
end
