class Comment < ActiveRecord::Base

    belongs_to :"post"
    validates_presence_of :post_id
    validates_presence_of :body
    validates_length_of :body, :in=> 10..100, :message => "El contenido del comentario es demasiado corto"

    belongs_to:post
end
